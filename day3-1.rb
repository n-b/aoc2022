#! ruby

sum = 0
File.readlines('day3.txt').each_with_index do |line, index|
  len = line.length
  a = line[..len/2-1]
  b = line[len/2..-2]

  a_items = a.chars
  b_items = b.chars
  
  item = (a_items & b_items).first
  
  priority = begin
    if item.ord <= 'Z'.ord
      item.ord - 'A'.ord + 27
    else
      item.ord - 'a'.ord + 1    
    end
  end 
  puts "#{item} : #{priority}"
  
  sum += priority
end

puts sum

