#! ruby

count = 0
max = 0
index = 0
index_max = 0
File.readlines('day1.txt').each do |line|
  if line == "\n"
    if count > max
      max = count
      index_max = index
    end
    count = 0
    index += 1
  else
    count += line.to_i
  end
end

puts "Of #{index} groups, the max is #{max} at index #{index_max}"