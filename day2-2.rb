#! ruby

# A X Rock
# B Y Paper
# C Z Scissors

# X lose
# Y draw
# Z win
score = 0
File.readlines('day2.txt').each_with_index do |line, index|
  op, goal = line.split.map(&:to_s)
  op = op.ord - 'A'.ord
  goal = goal.ord - 'X'.ord - 1
  me = 
  score += (op + goal)%3 +1

  fight = { -1 => 0, 0 => 3, 1 => 6}
  score += fight[goal]
end

puts "total: #{score}"