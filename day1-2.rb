#! ruby

scores = {}
count = 0

File.readlines('day1.txt').each_with_index do |line, index|
  if line == "\n"
    scores[index] = count
    count = 0
  else
    count += line.to_i
  end
end

puts scores.sort_by{|k, v| v}.last(3).to_h.values.sum