#! ruby

# A X Rock
# B Y Paper
# C Z Scissors

score = 0
File.readlines('day2.txt').each_with_index do |line, index|
  op, me = line.split.map(&:to_sym)
  
  own = {X: 1, Y: 2, Z: 3}
  score += own[me]
  
  fight = {
    [:A, :Y] => 6,
    [:B, :Z] => 6,
    [:C, :X] => 6,
    [:A, :X] => 3,
    [:B, :Y] => 3,
    [:C, :Z] => 3,
    [:A, :Z] => 0,
    [:B, :X] => 0,
    [:C, :Y] => 0
  }
  score += fight[[op, me]]

  puts "op: #{op} me #{me} own: #{own[me]} fight: #{fight[[op, me]]}"
end

puts "total: #{score}"