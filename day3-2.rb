#! ruby

sum = 0
group = []
File.readlines('day3.txt').each do |line|
  group << line
  if group.length == 3
    puts group
    item = group.map(&:chars).reduce(:&).first
    puts item.ord
      
    priority = begin
      if item.ord <= 'Z'.ord
        item.ord - 'A'.ord + 27
      else
        item.ord - 'a'.ord + 1    
      end
    end 
    puts "#{item} : #{priority}"
    
    sum += priority
    
    group = []
  end
end

puts sum

